# stm: Simple Task Manager

Python (<3) script to create simple tasks from the commandline. A task
is a simple text file which contains a title and a due timestamp.
Tasks are stored in the `.stm` directory below the current working
directory.

When invoked without arguments `stm` shows a list of (due) tasks, with
the title and the remaining time until due completion. When entering
tasks you can set the end date/time by several human-like time stamps,
like `2d` (48 hours from now) or `4wT15:00` (at 15:00 in four weeks).


## Installation

Copy the `stm` script to a directory set in the `$PATH` environment
variable while preserving the execute bit:

```bash
sudo cp -a stm /usr/local/bin/`
## or
cp -a stm ~/.local/bin/
```


## Usage

Get the list of available options:

```bash
stm --help
```

To get a list of tasks for the current directory, type:

```bash
stm
```


### Add task
 
 * Navigate to the directory for which you want to track tasks 
 * Add a new task which is due within 4 weeks at 3 PM

```bash
cd ~/someprojectdir
stm -n 'Publicize work' -d 4wT15:00
```


### Mark task as completed

 * Navigate to the directory containing tasks
 * Set status of task with id `1` to completed

```bash
cd ~/someprojectdir
stm -c 1
```


### Remove task

 * Navigate to the directory containing tasks
 * Set the status of the task with id `1` to removed (see `Purge task`
   to delete the actual task file)

```bash
cd ~/someprojectdir
stm -r 1
```


### Purge task

 NOTE: When purging a task, the actual task file is removed.

 * Navigate to the directory containing tasks
 * Remove the file containing the task with id `1`

```bash
cd ~/someprojectdir
stm -P 1
```
